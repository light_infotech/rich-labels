import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity, TouchableHighlight, ActivityIndicator, StyleSheet, Modal } from 'react-native';
import { Content, H2, Card, H3, Icon } from 'native-base';
import HeaderBar from './Common/HeaderBar';
import WooCommerceAPI from 'react-native-woocommerce-api';
import { Picker } from '@react-native-picker/picker';
import Favourites from './Common/Favourites';
import CartItems from './Common/CartItems';
import { useIsFocused } from '@react-navigation/native';

var apiCall = new WooCommerceAPI({
  url: 'https://richlabels.com',
  ssl: true,
  consumerKey: 'ck_9718a31d843961b38b01a3361d7a804352bb435b',
  consumerSecret: 'cs_89192ce144dd9d287cf224fea9a14d22946733c7',
  wpAPI: true,
  version: 'wc/v3',
  queryStringAuth: true
});

const watchImg = require('../assets/slicing/watch.png');

function Home(props) {

  const isFocused = useIsFocused();

  const [data, setData] = useState([{}]);
  const [loading, setLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [modelOptions, setModelOptions] = useState([]);
  const [item, setItem] = useState({});
  const [model, setModel] = useState(null);
  
  let productLists = data.map((item, i) => {
    return (
      <View key={i} style={{ margin: 5, width: '45%', backgroundColor: 'transparent', elevation: 0 }}>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity transparent onPress={() => {
            if (Favourites.some(product => product.slug === item.slug)) {
              var index = -1;
              Favourites.find(function (product, i) {
                if (product.name === item.name) {
                  index = i;
                }
              });
              if (index > -1) {
                Favourites.splice(index, 1)
                alert('The product is removed from your wishlist.');
                getProducts();
              }
            } else {
              Favourites.push(item);
              alert('The product is added into your wishlist.');
              getProducts();
            }
          }
          } >
            {Favourites.some(product => product.slug === item.slug) ? (
              <Icon
                name="heart"
                type="AntDesign"
                style={{ fontSize: 20, color: 'red' }}
              />
            ) : (
                <Icon
                  name="hearto"
                  type="AntDesign"
                  style={{ fontSize: 20, color: 'red' }}
                />
              )}
          </TouchableOpacity>
          <Text style={{ backgroundColor: 'black', color: 'white', position: 'absolute', right: 0, borderRadius: 5 }}>-10%</Text>
        </View>
        <Image
          style={{ alignSelf: 'center', width: 100, height: 100 }}
          source={{
            uri: item.images && item.images[0] && item.images[0].src,
          }} />
        <Text style={{ textAlign: 'center', fontWeight: 'bold', height: 40 }}>{item.name}</Text>
        <Text style={{ textAlign: 'center' }}>€ {item.price}</Text>

        <TouchableOpacity transparent
          onPress={() => {
            if (!CartItems.some(product => product.slug === item.slug)) {
              if (item.categories && item.categories[0] && item.categories[0].id == 52) {

                setModelOptions(item.attributes && item.attributes[0] && item.attributes[0].options);
                setModel(modelOptions[0]);
                setItem(item);
                setModalVisible(true);
              }
              else {
                CartItems.push(item);
                alert('The product is added into the cart.');
                getProducts();
              }
            }
          }
          }
          style={{ alignItems: 'center', borderWidth: 1, borderColor: 'black', borderRadius: 5, marginTop: 10, marginBottom: 10, height: 40 }}
        >
          {CartItems.some(product => product.slug === item.slug) ? (
            <View style={{ flexDirection: 'row', alignContent: 'center' }}>
              <Icon
                name="check"
                type="AntDesign"
                style={{ fontSize: 13, color: 'black', padding: 12, marginRight: -10 }}
              />
              <Text style={{ fontSize: 10, padding: 12 }}>
                ADDED TO BASKET</Text>
            </View>
          ) : (
              <View style={{ flexDirection: 'row', alignContent: 'center' }}>
                <Icon
                  name="handbag"
                  type="SimpleLineIcons"
                  style={{ fontSize: 13, color: 'black', padding: 12, marginRight: -10 }}
                />
                <Text style={{ fontSize: 10, alignSelf: 'center', padding: 12, }}>
                  ADD TO BASKET</Text>
              </View>
            )}

        </TouchableOpacity>
      </View>
    );
  });

  async function getProducts() {
    setLoading(true);
    apiCall.get('products', { featured: true, per_page: 100 })
      .then(data => {
        setData(data);
        setLoading(false);
      })
      .catch(error => {
        setLoading(false);
        alert(error);
      });
  }

  useEffect(() => {
    getProducts();
  }, [isFocused]);

  return (
    <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
      <HeaderBar props={props} />
      <H3 style={{ alignSelf: 'center', fontWeight: 'bold' }}>
        Redefined Luxury
        </H3>
      <View style={{ width: '90%', alignSelf: 'center' }}>

        <Modal
          style={{ marginTop: 50 }}
          animationType="fade"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(false);
          }}
        >
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Select Model</Text>

            <Picker
              style={{ marginBottom: 15, height: 50, width: 200, borderColor: "grey", borderWidth: 1 }}
              mode="dropdown"
              selectedValue={model}

              onValueChange={(itemValue) => setModel(itemValue)}>

              {modelOptions.map((item) => {
                return (<Picker.Item label={item} value={item} />)
              })}

            </Picker>

            <TouchableHighlight
              style={styles.openButton}
              onPress={() => {
                setModalVisible(!modalVisible);
                item.name = item.name + " - " + model;
                CartItems.push(item);
                alert('The product is added into the cart.');
                getProducts();

              }}
            >
              <Text style={styles.textStyle}>Submit</Text>
            </TouchableHighlight>
          </View>
        </Modal>

        <Text>Top Deals</Text>
        <Card
          style={{
            backgroundColor: '#f6f6f8',
            elevation: 0,
            margin: 10,
            padding: 5,
            borderRadius: 10,
          }}>
          <H3>Rolex Oyster Perpetual</H3>
          <Text style={{ color: '#707070' }}>39mm, Oystersteel</Text>
          <H2 style={{ marginTop: 8 }}>$259</H2>
          <Image
            source={watchImg}
            style={{ position: 'absolute', right: 0, width: 80, height: 80 }}
          />
        </Card>
      </View>
      <Content padder>
        <ScrollView style={{ width: '90%', alignSelf: 'center' }}>
          <Text>Featured Products</Text>
          {loading ? (
            <ActivityIndicator style={{ marginTop: 100 }} size="large" color="black" animating={loading} />
          ) : (
              <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                {productLists}
              </View>
            )}
        </ScrollView>
      </Content>
    </View>
  );
}

const styles = StyleSheet.create({
  modalView: {
    margin: 50,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "black",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    height: 40,
    width: 80
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center"
  }
});

export default Home;
