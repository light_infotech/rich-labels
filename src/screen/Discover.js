import React, { useEffect} from 'react';
import {View, Text, ScrollView, Image} from 'react-native';
import { Content, H2, H3 } from 'native-base';
import HeaderBar from './Common/HeaderBar';
import { useIsFocused } from '@react-navigation/native';

const shippingImg = require('../assets/slicing/truck.png');
const paymetImg = require('../assets/slicing/credit-card.png');
const returnRefendImg = require('../assets/slicing/returning.png');
const orderImg = require('../assets/slicing/delivery-box.png');
const voucharImg = require('../assets/slicing/gift-card.png');
const accountImg = require('../assets/slicing/user.png');


function Discover(props) {

  const isFocused = useIsFocused();
  
  useEffect(() => {
    console.log('Discover');
  }, [isFocused]);


  return (
    <ScrollView style={{flex: 1 , backgroundColor:'#ffffff'}}>
      <HeaderBar props = {props} />
      <Content padder>
        <H3 style={{alignSelf: 'center', fontWeight: 'bold'}}>
          Frequently Asked Questions
        </H3>
        <View style={{marginTop: 20}}>
          <View style={{flexDirection: 'row'}}>
            <Image source={shippingImg} />
            <H2 style={{marginLeft: 5, fontWeight: 'bold'}}>Shipping</H2>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
              How does Shipping Work
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
              We ship your order with DHL, Hermes and DPD. An order on
              Zalando.de can only be delivered to a delivery address in germany.
            </Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
            When does my order arrive?
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
            As soon as a package is sent, you will receive a shipping confirmation by email. You can use it to track the shipment history.
            </Text>
          </View>
        </View>
        <View style={{marginTop: 20}}>
          <View style={{flexDirection: 'row'}}>
            <Image source={paymetImg} />
            <H2 style={{marginLeft: 5, fontWeight: 'bold'}}>Payment</H2>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
            Why has my order not been sent yet?
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
            Please give us some time to prepare your order for shipping. We usually ship orders within 48 hours, some orders may take a little longer. 
            </Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
            My package should have been here long ago.
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
            Please check via tha shipment tracking whether the parcel was handed over to a neighbour. If it is not with neighbour and the shipping status has not changed for 5 days, Please contect us - you will find our contact.
            </Text>
          </View>
        </View>
        <View style={{marginTop: 20}}>
          <View style={{flexDirection: 'row'}}>
            <Image source={returnRefendImg} />
            <H2 style={{marginLeft: 5, fontWeight: 'bold'}}>Returns & Refends</H2>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
            How do i sent an article back?
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
            We ship your order with DHL, Hermes and DPD. An order on zalendo.de can only be delivered on a delivery address in germony.
            </Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
            My package should have here long time!
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
            Please check via the shipment trackin whether the parcel was handed over to a neighbour. It is not with a neighbour and the shipping status has not changed for 5
            </Text>
          </View>
        </View>
        <View style={{marginTop: 20}}>
          <View style={{flexDirection: 'row'}}>
            <Image source={orderImg} />
            <H2 style={{marginLeft: 5, fontWeight: 'bold'}}> Order</H2>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
            How do i send back an article?
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
            We ship your order with DHL,Hermes and DPD. An order on zalendo.de can only be delivered on a delivery address in Germony.
            </Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
            How do i get a new return label?
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
            With express delivery, your order will be delivered in 1-2  working days for a fee of €7.89. Simply select this deliverey option when you complete your order under "shipping option". 
            </Text>
          </View>
        </View>
        <View style={{marginTop: 20}}>
          <View style={{flexDirection: 'row'}}>
            <Image source={voucharImg} />
            <H2 style={{marginLeft: 5, fontWeight: 'bold'}}> Vouchers</H2>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
            How do i send a article back?
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
            We ship your order with DHL,Hermes and DPD. An order on zalendo.de can only be delivered on a delivery address in Germony.
            </Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
            How do i get a new return label?
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
            With express delivery, your order will be delivered in 1-2  working days for a fee of €7.89. Simply select this deliverey option when you complete your order under "shipping option". 
            </Text>
          </View>
        </View>
        <View style={{marginTop: 20}}>
          <View style={{flexDirection: 'row'}}>
            <Image source={accountImg} />
            <H2 style={{marginLeft: 5, fontWeight: 'bold'}}> My customer Account:</H2>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
            How does shipping work?
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
            We ship your order with DHL,Hermes and DPD. An order on zalendo.de can only be delivered on a delivery address in Germony.
            </Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
            When does my order arrive?
            </Text>
            <Text style={{color: '#43454a', marginTop: 5}}>
            As soon as a package is send you will receive a shipping option.
            </Text>
          </View>
        </View>
      </Content>
    </ScrollView>
  );
}

export default Discover;
