const initState = {
    error: null,
    clubs: [],
    msg:null
}
const clubsReducer = (state = initState, action) => {
    switch (action.type) {
        case 'GET_CLUBS_SUCCESS':
            return {
                ...state,
                error: null,
                clubs: action.clubs,
                msg :action.msg
            }
        case 'GET_CLUBS_ERROR':
            return {
                ...state,
                error: action.msg,
                clubs: []
            }
        default:
            return state
    }
}
export default clubsReducer 