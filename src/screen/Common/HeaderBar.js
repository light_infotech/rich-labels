import React from 'react';
import { Text,  Image , TouchableOpacity} from 'react-native';
import {
  Header,
  Left,
  Right,
  Icon,
  Badge,
  Body,
} from 'native-base';
const IconImg = require('../../assets/slicing/icon.png');
import CartItems from './CartItems';

export default function HeaderBar({ props }){
  return (
      <Header style={{ backgroundColor: 'black', height: 100 }}>
        <Left>
          <TouchableOpacity onPress={() => props.navigation.openDrawer()}>
            <Icon
              type="Entypo"
              name="menu"
              style={{ marginLeft: 10, fontSize: 35, color: "white" }}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ alignItems: 'flex-end' }}>
          <Image source={IconImg} />
        </Body>
        <Right>
          <TouchableOpacity onPress={() => props.navigation.navigate('card')}>
            <Icon
              type="FontAwesome"
              name="shopping-bag"
              style={{ marginRight: 10, fontSize: 30, color: "white" }}
            />
          </TouchableOpacity>

          <Badge style={{ backgroundColor: 'red', height: 20, width: 20, marginLeft: -20, marginBottom: 20 }}>
            <Text style={{ color: 'white' }}>{CartItems.length}</Text>
          </Badge>
          
        </Right>
      </Header>
  )
}