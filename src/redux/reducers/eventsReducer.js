const initState = {
    error:null,
   events:[]
  }
  
  const eventsReducer = (state = initState, action) => {
    switch(action.type){
      
        case 'GET_EVENTS_ERROR':
          return {
            ...state,
            error:action.msg,
            events:[]
          }
          case "GET_EVENTS_SUCCESS":
            return {
              ...state,
              error: null,
              events: action.events,
              msg :action.msg
            }
        default:
          return state
    } 
  }
  
  export default eventsReducer 