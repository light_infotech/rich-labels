import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, Image , TouchableOpacity } from 'react-native';
import {
  Container,
  Header,
  Col,
  Grid,
  Content,
  H2,
  H1,
  Card,
  Left,
  Item,
  Input,
  Segment,
  Right,
  Button,
  Icon,
  Title,
  Thumbnail,
  CardItem,
  Body,
  H3,
} from 'native-base';
import { connect } from 'react-redux';

function ProductsList(props) {
  const [isToggled, setToggled] = useState(false);
  const [state, setState] = useState({
    category:"all",
    // data: [
    //   {name: 'Phone Case ' , image: require('../assets/products/1.jpg')},
    //   {name: 'Airpod Cases', image: require('../assets/products/2.jpg')},
    //   {name: 'Wallet', image: require('../assets/products/3.jpg')},
    //   {name: 'Card Holder', image: require('../assets/products/4.jpg')},
    //   {name: 'Phone Case Bags', image: require('../assets/products/1.jpg')},
    //   {name: 'Watch Band', image: require('../assets/products/2.jpg')},
    //   {name: 'Accessoires', image: require('../assets/products/3.jpg')},
    // ],
  });
  // console.log('products', props.products)
  const SelectedCateGory = (value)=>{
    console.log('selected value', value)
    setState((state)=>({
...state , category:value
    }))
  }
  const toggleTrueFalse = () => setToggled(!isToggled);
  let ShowList = state.data.map((item, i) => {
    // console.log('items', item)
    return (
      <View key={i}  style={{}}>
        <Card style={{width:100 , backgroundColor:'red' , display:'flex'  , flexDirection:'row', flexWrap:"wrap" }}>
          <CardItem>
            <Text >12</Text>
          </CardItem>
        </Card>
        {/* <TouchableOpacity onPress={()=> props.navigation.navigate('Single', {Show:item})} >
                  <Card style={{margin: 10, borderRadius: 20 , width:150}}>
           <CardItem cardBody style={{margin: 10, borderRadius: 20}}>
          
            <Image
              source={item.image}
              style={{height: 250, width: null, flex: 1, borderRadius: 20}}
            />
          </CardItem> 
          <View style={{margin: 20 }}>
            <H2 style={{fontFamily: 'Sarabun-SemiBold' }}>{item.name}</H2>
            <View style={{display: 'flex', flexDirection: 'row', top: 10}}>
              <Thumbnail
                source={item.image}
                style={{width: 20, height: 20, marginRight: 10}}
              />
            
              <Icon
                name="dollar-sign"
                type="FontAwesome5"
                style={{fontSize: 18}}
              />
              <Text style={{fontFamily: 'Sarabun-SemiBold', marginLeft: 10}}>
                400
              </Text>

              <View style={{position: 'absolute', right: 0}}>
                <Icon
                  name="heart"
                  type="FontAwesome5"
                  style={{color: 'red', fontSize: 22}}
                />
              </View>
            </View>
          </View>
        </Card></TouchableOpacity> */}

      </View>
    );
  });

  return (
    <View style={{flex:1 , backgroundColor:'white'}} >
      <View>
      <Header style={{backgroundColor: '#FFFFFF'}}>
        <Left>
          <Button transparent onPress={()=>props.navigation.openDrawer()}>
            <Icon name="bars" type="FontAwesome5" style={{color: 'black'}} />
          </Button>
        </Left>
        <Body>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Shop </Text>
        </Body>
        <Right>
          {isToggled === true ? (
            <Item>
              <Icon name="search" type="FontAwesome5" style={{fontSize: 16}} />
              <Input placeholder="Search" />
              <Icon
                onPress={() => toggleTrueFalse()}
                name="times"
                type="FontAwesome5"
                style={{fontSize: 16}}
              />
            </Item>
          ) : (
            <View style={{display: 'flex', flexDirection: 'row'}}>
              <Icon
                onPress={() => toggleTrueFalse()}
                name="search"
                type="FontAwesome5"
                style={{marginRight: 25, fontSize: 16}}
              />
              <Icon
              onPress={()=>props.navigation.navigate('card')}
                name="shopping-cart"
                type="FontAwesome5"
                style={{fontSize: 16}}
              />
            </View>
          )}
        </Right>
      </Header>
      <ScrollView
        style={{marginTop: 10, marginBottom: 10, height:40 }}
        horizontal={true}
         showsHorizontalScrollIndicator={false} 
        > 
        <TouchableOpacity onPress={()=>SelectedCateGory("all")}
          style={{
            borderRadius: 10,
            // backgroundColor: 'white', 
            justifyContent: 'center', 
          
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>ALL </Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=>SelectedCateGory('phone_case')}
          style={{
            borderRadius: 10,
            // backgroundColor: 'white', 
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>Phone Case</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=>SelectedCateGory('airpod_case')}
          style={{
            borderRadius: 10,
            // backgroundColor: 'white',
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>Airpod Cases</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=>SelectedCateGory('wallet')}
          style={{
            borderRadius: 10,
            // backgroundColor: 'white',
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>Wallet</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>SelectedCateGory('card_holder')}
          style={{
            borderRadius: 10,
            // backgroundColor: 'white', 
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>Card Holder</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>SelectedCateGory('phone_case')}
          style={{
            borderRadius: 10,
            // backgroundColor: 'white', 
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>Phone Case Bags</Text>
        </TouchableOpacity>
        <TouchableOpacity   onPress={()=>SelectedCateGory('Watch_band')}
          style={{
            borderRadius: 10,
            // backgroundColor: 'white', 
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}> Watch Band</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            borderRadius: 10,
            // backgroundColor: 'white', 
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}> Accessoires</Text>
        </TouchableOpacity>
      </ScrollView>
      </View>
      {/* <ScrollView >{ShowList}</ScrollView>  */}
      <View style={{flex:1}}>
      <Content padder >
      {state.category === "all"  ? <View   style={{  flexDirection: "row", flexWrap: 'wrap',  alignItems: "flex-start" }}>
          {props.products.map((item, i) =>
           <TouchableOpacity key={i} style={{backgroundColor:'white' , margin: 5, width:'47%' , elevation:6}} onPress={()=> props.navigation.navigate('Single', {Show:item})}>
            <View style={{position: 'absolute', right: 5 , top:10 , alignItems:'center' , justifyContent:'center' , zIndex:1 , backgroundColor:'white' , width:30 , height:30, borderRadius:30/2 , }}>
                <Icon
                  name="heart"
                  type="FontAwesome5"
                  style={{color: 'red', fontSize: 22}}
                />
              </View>
            <Image
              source={item.image}
              resizeMode="stretch"
              style={{ width:'100%', height:150 , borderRadius: 0}}
            />
           
           <H3 style={{padding:0, marginLeft:10 }}>{item.name}</H3>
           <Text style={{padding:0, marginLeft:10}}>$100</Text>
            </TouchableOpacity>
         
           )}
      </View>  :
       <View   style={{  flexDirection: "row", flexWrap: 'wrap',  alignItems: "flex-start" }}>
       {props.products.filter(i => i.category === state.category ).map((item, i) =>
        <TouchableOpacity key={i} style={{backgroundColor:'white' , margin: 5, width:'47%' , elevation:6}} onPress={()=> props.navigation.navigate('Single', {Show:item})} >
          
         <View style={{position: 'absolute', right: 5 , top:10 , alignItems:'center' , justifyContent:'center' , zIndex:1 , backgroundColor:'white' , width:30 , height:30, borderRadius:30/2 , }}>
             <Icon
               name="heart"
               type="FontAwesome5"
               style={{color: 'red', fontSize: 22}}
             />
           </View>
         <Image
           source={item.image}
           resizeMode="stretch"
           style={{ width:'100%', height:150 , borderRadius: 0}}
         />
        
        <H3 style={{padding:0, marginLeft:10}}>{item.name}</H3>
        <Text style={{padding:0, marginLeft:10}}>$100</Text>
         </TouchableOpacity>
      
        )}
   </View> }
      </Content></View>
    </View>
  );
}
const mapStateToProps = state => {
  return {
    products: state.products.products,
   
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getMyAllCustomers: () => dispatch(getMyAllCustomers()),
    getOrderNumber: () => dispatch(getOrderNumber()),
    getSelectedProducts: data => dispatch(getSelectedProducts(data)),
    addOrder: data => dispatch(addOrder(data))
  };
};
export default connect(mapStateToProps , null)(ProductsList);
