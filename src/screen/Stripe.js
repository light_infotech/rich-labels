import React , {useState} from 'react';
import {View, Text, Button, TouchableOpacity} from 'react-native';
import {CreditCardInput} from 'react-native-input-credit-card';
import Stripe from 'react-native-stripe-api';


// import Stripe from 'react-native-stripe-api';
// const apiKey ='pk_test_51HvQIGIiAw8FQvzpZxg2NOsnFkg8ZJ308W4xSEZ4KRs4xtXJIrIUzazmmtlcJcI7jZiJ84wXCs619QJaeUsuzFJw00wFkHKudS';
// const client = new Stripe(apiKey);
export default function StripePayment() {
  const [state, setState] = useState({
    cardInfo: {
      status: {
        cvc: 'incomplete',
        expiry: 'incomplete',
        number: 'incomplete',
      },
      valid: false,
      values: {
        cvc: '',
        expiry: '',
        number: '',
        type: undefined,
      },
    },
  });
  const onChangeCard = (form) => {
    setState({...state, cardInfo: form});
  };
  const pay = async()=>{
    console.log('state', state)
    if(state.cardInfo.valid)
    {
    //  const apiKey = 'pk_live_51H0AzDD131s24SI9XX0Qi4VnR3vrUkLNnlOIwjOGj2kQNEYtb4ItPcTp3F7Pa8sQ6LAqzIkscoN5OuQEiJtvqKDN00x3uI2kEM'
      const apiKey ='pk_test_51HvQIGIiAw8FQvzpZxg2NOsnFkg8ZJ308W4xSEZ4KRs4xtXJIrIUzazmmtlcJcI7jZiJ84wXCs619QJaeUsuzFJw00wFkHKudS';

    const client = new Stripe(apiKey)
    
    // Create a Stripe token with new card infos
    const expiry = state.cardInfo.values.expiry.split('/');
       const token = await client.createToken({
           number: state.cardInfo.values.number,
           exp_month: expiry[0], 
           exp_year: expiry[1], 
           cvc: state.cardInfo.values.cvc
        })
        console.log('token', token)
      }else {
        alert('Card Information not valid')
      }
  }
  return (
    <View style={{backgroundColor:'#FFFFFF' }}>
      <CreditCardInput onChange={onChangeCard} />
      <TouchableOpacity onPress={pay} style={{backgroundColor:'black' , width:'80%' , alignSelf:'center'}}>
        <Text style={{color:'white', paddingTop:10, paddingBottom:10 , alignSelf:'center', fontWeight:'bold'}}>Pay Now</Text>
      </TouchableOpacity>
    </View>
  );
}
