import React, { useState, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity, TouchableHighlight, StyleSheet, Modal } from 'react-native';
import { Content, H3, Icon } from 'native-base';
import { Picker } from '@react-native-picker/picker';
import HeaderBar from './Common/HeaderBar';
import Favourites from './Common/Favourites';
import useForceUpdate from './Common/ForceUpdate';
import CartItems from './Common/CartItems';
import { useIsFocused } from '@react-navigation/native';
const delteImg = require('../assets/slicing/delete.png');

function WishList(props) {

  const [modalVisible, setModalVisible] = useState(false);
  const [modelOptions, setModelOptions] = useState([]);
  const [item, setItem] = useState({});
  const [model, setModel] = useState(null);

  const isFocused = useIsFocused();
  const forceUpdate = useForceUpdate();

  let productLists = Favourites.map((item, i) => {
    return (
      <View key={i} style={{ margin: 5, width: '45%', backgroundColor: 'transparent', elevation: 0 }}>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity transparent onPress={() => {
            var index = -1;
            Favourites.find(function (product, i) {
              if (product.name === item.name) {
                index = i;
              }
            });
            if (index > -1) {
              Favourites.splice(index, 1);
              forceUpdate();
            }
          }} style={{ width: 16, height: 20 }} >
            <Image source={delteImg} style={{ width: 16, height: 20 }} />
          </TouchableOpacity>
          <Text style={{ backgroundColor: 'black', color: 'white', position: 'absolute', right: 0, borderRadius: 5 }}>-10%</Text>
        </View>
        <Image
          style={{ alignSelf: 'center', width: 100, height: 100 }}
          source={{
            uri: item.images && item.images[0] && item.images[0].src,
          }} />
        <Text style={{ textAlign: 'center', fontWeight: 'bold', height: 40 }}>{item.name}</Text>
        <Text style={{ textAlign: 'center' }}>€ {item.price}</Text>
        <TouchableOpacity transparent
          onPress={() => {
            if (!CartItems.some(product => product.slug === item.slug)) {
              if (item.categories && item.categories[0] && item.categories[0].id == 52) {

                setModelOptions(item.attributes && item.attributes[0] && item.attributes[0].options);
                setModel(modelOptions[0]);
                setItem(item);
                setModalVisible(true);
              }
              else {
                CartItems.push(item);
                alert('The product is added into the cart.');
                forceUpdate();
              }
            }
          }
          }
          style={{ borderWidth: 1, borderColor: 'black', borderRadius: 5, marginTop: 10, marginBottom: 10 }}
        >
          {CartItems.some(product => product.slug === item.slug) ? (
            <View style={{ flexDirection: 'row', alignContent: 'center' }}>
              <Icon
                name="check"
                type="AntDesign"
                style={{ fontSize: 13, color: 'black', padding: 12, marginRight: -10 }}
              />
              <Text style={{ fontSize: 10, padding: 12 }}>
                ADDED TO BASKET</Text>
            </View>
          ) : (
              <View style={{ flexDirection: 'row', alignContent: 'center' }}>
                <Icon
                  name="handbag"
                  type="SimpleLineIcons"
                  style={{ fontSize: 13, color: 'black', padding: 12, marginRight: -10 }}
                />
                <Text style={{ fontSize: 10, alignSelf: 'center', padding: 12, }}>
                  ADD TO BASKET</Text>
              </View>
            )}

        </TouchableOpacity>
      </View>
    );
  });

  useEffect(() => {
    forceUpdate();
  }, [isFocused]);

  return (
    <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
      <HeaderBar props={props} />

      <Modal
        style={{ marginTop: 50 }}
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
      >
        <View style={styles.modalView}>
          <Text style={styles.modalText}>Select Model</Text>

          <Picker
            style={{ marginBottom: 15, height: 50, width: 200, borderColor: "grey", borderWidth: 1 }}
            mode="dropdown"
            selectedValue={model}

            onValueChange={(itemValue) => setModel(itemValue)}>

            {modelOptions.map((item) => {
              return (<Picker.Item label={item} value={item} />)
            })}

          </Picker>

          <TouchableHighlight
            style={styles.openButton}
            onPress={() => {
              setModalVisible(!modalVisible);
              item.name = item.name + " - " + model;
              CartItems.push(item);
              alert('The product is added into the cart.');
              forceUpdate();
            }}
          >
            <Text style={styles.textStyle}>Submit</Text>
          </TouchableHighlight>
        </View>
      </Modal>

      <Content padder>
        <H3 style={{ alignSelf: 'center', fontWeight: 'bold', marginBottom: 20 }}>
          My WishList
        </H3>
        <View style={{ width: '90%', alignSelf: 'center' }}>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            {productLists}
          </View>
        </View>
      </Content>
    </View>
  );
}

const styles = StyleSheet.create({
  modalView: {
    margin: 50,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "black",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    height: 40,
    width: 80
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    fontWeight: "bold",
    fontSize: 20,
    textAlign: "center"
  }
});


export default WishList;
