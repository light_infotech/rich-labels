const initState = {
    error: null,
    
    products: [
        {id:1,name:'Phone Case', category:"phone_case", image:require('./../../assets/products/phone-case.jpg')},
        {id:2,name:'Airpod Casces', category:"airpod_case", image:require('./../../assets/products/airpod-cases.jpg')},
        {id:3,name:'Wallet', category:"wallet", image:require('./../../assets/products/wallet.jpg')},
        {id:4,name:'Card Holder', category:"card_holder", image:require('./../../assets/products/card-holder.jpg')},
        {id:5,name:'Phone Case Bags', category:"phone_case", image:require('./../../assets/products/phone-case-bags.jpg')},
        {id:6,name:'Watch Band', category:"Watch_band", image:require('./../../assets/products/watch-band.jpg')},
        {id:7,name:'Wallet', category:"Watch_band", image:require('./../../assets/products/1.jpg')},
        {id:8,name:'Wallet', category:"Watch_band", image:require('./../../assets/products/2-1.jpg')},
        {id:9,name:'Wallet', category:"Watch_band", image:require('./../../assets/products/3-1.jpg')},

],
    msg:null
}

const clubsReducer = (state = initState, action) => {
    switch (action.type) {
        case 'GET_PRODUCTS_SUCCESS':
            return {
                ...state,
                error: null,
                clubs: action.clubs,
                msg :action.msg
            }
        case 'GET_PRODUCTS_ERROR':
            return {
                ...state,
                error: action.msg,
                clubs: []
            }
        default:
            return state
    }
}
export default clubsReducer 