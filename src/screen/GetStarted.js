import React from "react";
import { View, Dimensions,   Text, } from "react-native";
import {
  Container,
  Header,
  Content,
  Badge,

  Icon,
  H2,
  Button,
} from "native-base";

export default function GetStarted(props) {
  return (
    <View
      style={{
        backgroundColor: "black",
        alignItems: "center",
        justifyContent: "center",
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height,
      }}
    >
      <View
        style={{
          width: 100,
          height: 100,
          borderColor: "white",
          borderWidth: 1,
          borderRadius: 100 / 2,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Icon name="camera" type="FontAwesome5" style={{ color: "white" }} />
        <Icon
          name="plus"
          type="FontAwesome5"
          style={{
            color: "black",
            backgroundColor: "white",
            position: "absolute",
            bottom: 0,
            borderRadius: 100,
            fontSize: 20,
            right: 12,
          }}
        />
      </View>
      <H2 style={{ color: "white", marginTop: 20 }}>Wellocme, Mujahid</H2>
      <Text style={{ color: "white", alignSelf: "center" }}>
        {" "}
        In order to personalize your Nike app
      </Text>
      <Text style={{ color: "white", alignSelf: "center" }}>
        {" "}
        experience. we want to get to know{" "}
      </Text>
      <Text style={{ color: "white", alignSelf: "center" }}>you better.</Text>
      <View style={{ position: "absolute", bottom: 20 }}>
        <Button rounded style={{ backgroundColor: "white" }} onPress={()=>props.navigation.navigate('interest')}>
          <Text style={{paddingLeft:15, paddingRight:15}}>Get Started </Text>
        </Button>
      </View>
    </View>
  );
}
