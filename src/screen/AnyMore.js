import React, { useState, useEffect } from "react";
import { View, Text, ScrollView, Image } from "react-native";
import {
  Container,
  Header,
  Col,
  Grid,
  Content,
  H2,
  H1,
  Card,
  Left,
  Item,
  Input,
  Segment,
  Right,
  Button,
  Icon,
  Title,
  CardItem,
  Body,
} from "native-base";

function AnyMore(props) {
  const [state, setState] = useState({
    data: [
      { name: "Phone Case", image: require("../assets/products/1.jpg") },
      { name: "Airpod Cases", image: require("../assets/products/2.jpg") },
      { name: "Wallet", image: require("../assets/products/3.jpg") },
      { name: "Card Holder", image: require("../assets/products/4.jpg") },
      { name: "Phone Case Bags", image: require("../assets/products/4.jpg") },
      { name: "Watch Band", image: require("../assets/products/4.jpg") },
      { name: "Accessoires", image: require("../assets/products/4.jpg") },



    ],
  });
  let ShowList = state.data.map((item, i) => {
    return (
      <Card key={i} style={{ borderRadius: 30}}>
        <CardItem
          style={{
             flexDirection: "column",
            display: "flex",
           
            borderRadius: 30,
            elevation: 4,
            backgroundColor:'black'          }}
        >
        
          <Image
            source={item.image}
            style={{ width: "100%", height: 150, borderRadius: 30 }}
          />
            <H2
            style={{
          color:'#FFFFFF',
              marginLeft: 10,
              fontWeight: "bold",
              zIndex: 1,
            }}
          >
            {item.name}
          </H2>
        </CardItem>
      </Card>
    );
  });
  return (
    <View style={{ flex: 1 , backgroundColor:'black'}}>
      <Header style={{backgroundColor:'black'}}>
        <Left />
        <Body>
          <Title style={{color:'#FFFFFF'}}>Any More?</Title>
        </Body>
        <Right />
      </Header>
      <Content padder>{ShowList}</Content>
      <View style={{ position: "absolute", bottom: 20 ,  alignSelf:'center'}}>
        <Button rounded style={{ backgroundColor: "white"}} onPress= { ()=> props.navigation.navigate('productList')}>
          <Text  style={{paddingLeft:25, paddingRight:25, fontWeight:'bold'}}>Next </Text>
        </Button>
      </View>
    </View>
  );
}

export default AnyMore;
