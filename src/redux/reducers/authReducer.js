
const initState = {
  authError: null,
  user:{},
  isAuthenticated: false,
  error:false ,
  msg:""
}

const authReducer = (state = initState, action)  => {
 
  switch(action.type){
    case 'LOGIN_ERROR':
    return {
      ...state,
      authError: action.msg,
      error:!state.error
    } 
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        authError:null,
        user:action.user,
        isAuthenticated: true,
      }
      case 'LOGOUT_USER':
      return {
        ...state,
        authError:null,
        user:{},
        isAuthenticated: false,
       
      }
      case "GET_CURRENT_USER":{
        
        return {
          ...state,
        user:action.payload
        }
      }
      case 'SIGNUP_ERROR':
       
    return {
      ...state,
      authError: action.msg,
      error:!state.error
    } 
    case 'SIGNUP_SUCCESS':
     
      return {
        ...state,
        authError:null,
        user:action.user,
        isAuthenticated: true,

      }
      default:
        return state
  } 

}

export default authReducer 