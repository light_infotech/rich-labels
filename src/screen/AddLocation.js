import React from "react";
import { View, Text, Dimensions } from "react-native";
import {
  Container,
  Header,
  Content,
  Badge,
  Icon,
  H2,
  Button,
} from "native-base";
export default function AddLocation() {
  return (
    <View
      style={{
        backgroundColor: "black",
        alignItems: "center",
        justifyContent: "center",
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height,
      }}
    >
      <View>
        <Icon
          name="check-circle"
          type="FontAwesome5"
          style={{ color: "white" , fontSize:50 }}
        />
      </View>
      <View  style={{alignItems:'center'}}>
        <H2 style={{ color: "white", padding:10 }}>Update Your location</H2>
        <Text style={{ color: "white" , textAlign:"left", padding:15}}>
          For the best experience in the Nick app, select a supported
          company/region as your location. if your country/region is not
          supported, visit. Nick.com
        </Text>
      </View>
      <View style={{ position: "absolute", bottom: 20 ,  }}>
        <Button rounded style={{ backgroundColor: "white",   }}>
          <Text  style={{paddingLeft:25, paddingRight:25}}>Continue </Text>
        </Button>
      </View>
    </View>
  );
}
