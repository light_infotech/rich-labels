import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, Image } from 'react-native';
import { Content, Button, Icon, H3 } from 'native-base';
import HeaderBar from './Common/HeaderBar';
import CartItems from './Common/CartItems';
import { TouchableOpacity } from 'react-native-gesture-handler';
import useForceUpdate from './Common/ForceUpdate';
import { useIsFocused } from '@react-navigation/native';

function AddToCard(props) {
  const [total, setTotal] = useState(0);
  const [lineItems, setLinesItems] = useState([{}]);
  const forceUpdate = useForceUpdate();
  const isFocused = useIsFocused();

  function addCounter() {
    if (CartItems.length > 0) {
      for (let i = 0; i < CartItems.length; i++) {
        if (!CartItems[i].count)
          CartItems[i].count = 1;
      }
    }
  }
 
  function getLineItems() {
    var items = new Array();
    for (let i = 0; i < CartItems.length; i++) {
      items.push({
        product_id: CartItems[i].id,
        name: CartItems[i].name,
        quantity: CartItems[i].count,
        subtotal: (CartItems[i].price * CartItems[i].count).toString(),
        price: parseFloat(CartItems[i].price)
      })
    }

    setLinesItems(items);
  }

  const getTotal = () => {
    let temp = 0;

    for (let i = 0; i < CartItems.length; i++) {
      temp = temp + (CartItems[i].price * CartItems[i].count);
    }

    temp = temp.toFixed(2);
    setTotal(temp);
    console.log('temp', temp)
  }

  let productLists = CartItems.map((item, i) => {
    return (
      <View key={i} style={{
        flex: 1,
        alignSelf: 'center',
        flexDirection: 'row',
        marginTop: 20,
        borderStyle: 'solid',
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 15
      }}>
        <View style={{ margin: 10, flex: 3 }}>
          <Image
            style={{ alignSelf: 'center', width: 100, height: 100 }}
            source={{
              uri: item.images && item.images[0] && item.images[0].src,
            }} />
        </View>
        <View style={{ flex: 7, margin: 10 }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 8 }}>
              <Text style={{ color: '#707070' }}>{item.name}</Text>
            </View>
            <View style={{ flex: 2 }}>
              <TouchableOpacity transparent
                style={{ right: -10  }}
                onPress={() => {
                  var index = -1;
                  CartItems.find(function (product, i) {
                    if (product.name === item.name) {
                      index = i;
                    }
                  });
                  if (index > -1) {
                    CartItems.splice(index, 1);
                    forceUpdate();
                    getTotal();
                    getLineItems();
                  }
                }}>
                <Icon
                  type="MaterialIcons"
                  name="delete-forever"
                  style={{ marginBottom: 20, fontSize: 23, color: "red" }}
                />
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 6 }}>
              <Text style={{ fontWeight: 'bold', fontSize: 20 }}>€ {item.price}</Text>
            </View>
            <View style={{ flex: 4 }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', right: 10, bottom: 0 }}>
                <Button transparent>
                  <Icon
                    name="minus"
                    type="FontAwesome5"
                    style={{
                      color: 'black',
                      fontSize: 18
                    }}
                    onPress={() => {
                      if (item.count > 1) {
                        item.count = item.count - 1, getTotal();
                        getLineItems();
                      }
                    }}
                  />
                </Button>
                <Text>{item.count}</Text>
                <Button transparent>
                  <Icon
                    name="plus"
                    type="FontAwesome5"
                    style={{
                      color: 'black',
                      fontSize: 18
                    }}
                    onPress={() => { 
                      item.count = item.count + 1, getTotal();
                       getLineItems(); 
                      }}
                  />
                </Button>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  });

  useEffect(() => {
    addCounter();
    getTotal();
    getLineItems();
  }, [isFocused]);

  return (
    <View style={{ flex: 1, backgroundColor: '#f6f6f8' }}>
      <HeaderBar props={props} />
      <H3 style={{ marginTop: 10, alignSelf: 'center', fontWeight: 'bold' }}>CART</H3>
      <ScrollView style={{ flex: 7 }}>
        <Content padder >
          {productLists}
        </Content>
      </ScrollView>
      <View style={{ bottom: -20, backgroundColor: 'black', width: '100%', borderRadius: 20 }}>
        <View style={{ flexDirection: 'row', justifyContent: "space-around", marginTop: 20 }}>
          <Text style={{ color: 'white' }}>Subtotal</Text>
          <Text style={{ color: 'white' }}>€ {total}</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: "space-around", marginTop: 4, marginBottom: 4 }}>
          <Text style={{ color: 'white' }}>DHL Shipping</Text>
          <Text style={{ color: 'white' }}>€ 3.55</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: "space-around", marginTop: 10, marginBottom: 10 }}>
          <Text style={{ color: 'white', fontWeight: 'bold' }}>Total to Pay</Text>
          <Text style={{ color: 'white' }}>
            € {(
              parseFloat(total) + parseFloat(3.55)
            ).toFixed(2)}
          </Text>
        </View>
        <TouchableOpacity
          style={{
            backgroundColor: 'white',
            marginBottom: 30,
            width: '60%',
            alignSelf: 'center',
            borderRadius: 10,
            marginTop: 10
          }}
          onPress={() => {
            props.navigation.navigate('order', {
              total: parseFloat(total) + parseFloat(3.55),
              lineItems: lineItems
            })
          }}>
          <Text style={{ alignSelf: 'center', paddingTop: 10, paddingBottom: 10, fontWeight: 'bold' }}>Proceed to checkout</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default AddToCard;
