import * as React from "react";
import { View, Text, Image, TouchableOpacity, ScrollView, StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { Icon } from "native-base";
import Home from "../screen/Home";
import Discover from "../screen/Discover";
import AddToCard from "../screen/Card";
import Order from "../screen/OrderDetails";
import Contact from "../screen/ContactUs";
import WishList from "../screen/WishList";
import Category from "../screen/Category";
 import StripePayment from "../screen/Stripe";



const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

function Cart() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}>
      <Stack.Screen
        name="addToCard"
        component={AddToCard} />
      <Stack.Screen
        name="order"
        component={Order} />
          <Stack.Screen
        name="payment"
        component={StripePayment} /> 
    </Stack.Navigator>
  );
}

function AppBottomNavigator() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        activeTintColor: '#eba10c',
        inactiveTintColor: 'gray',
        activeBackgroundColor: 'black',
        inactiveBackgroundColor: 'black'
      }}
    >
      <Tab.Screen
        name="home"
        component={Home}
        options={{
          tabBarIcon: ({ color }) => <Icon type="FontAwesome" name="home" style={{ fontSize: 25, color: color }} />,
        }} />

      <Tab.Screen
        name="card"
        component={Cart}
        options={{
          tabBarIcon: ({ color }) => <Icon type="FontAwesome5" name="clipboard-list" style={{ fontSize: 25, color: color }} />,
        }} />

      <Tab.Screen
        name="likes"
        component={WishList}
        options={{
          tabBarIcon: ({ color }) => <Icon type="AntDesign" name="heart" style={{ fontSize: 25, color: color }} />,
        }} />

      <Tab.Screen
        name="share"
        component={Contact}
        options={{
          tabBarIcon: ({ color }) => <Icon type="Entypo" name="paper-plane" style={{ fontSize: 25, color: color }} />,
        }} />

      <Tab.Screen
        name="QA"
        component={Discover}
        options={{
          tabBarIcon: ({ color }) => <Icon type="MaterialIcons" name="question-answer" style={{ fontSize: 25, color: color }} />,
        }} />

    </Tab.Navigator>
  );
}
function AppDrawerNavigator() {
  return (
    <Drawer.Navigator drawerContent={(props) => {
      return (
        <View style={{ flex: 1 }}>
          <View
            style={{
              backgroundColor: "black",
              height: 100,
              alignItems: "center",
              justifyContent: "center",
              flexDirection: 'row'
            }}
          >
            <Image
              source={require("../assets/slicing/logo.png")}
              width={30}
              height={30}
            />
            <TouchableOpacity onPress={() => props.navigation.closeDrawer()}>
              <Icon
                type="Entypo"
                name="cross"
                style={{ marginLeft: 10, fontSize: 40, color: "white" }}
              />
            </TouchableOpacity>
          </View>

          <ScrollView>
            <TouchableOpacity style={styles.drawerButton}
              onPress={() => props.navigation.navigate('Category', { title: 'Watch Bands', id: 31 })}>
              <Text style={styles.drawerText}>Watch Bands</Text>
              <Image
                style={styles.drawerImage}
                source={{
                  uri: 'https://richlabels.com/wp-content/uploads/2020/05/DSC_3847-scaled.jpg',
                }} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.drawerButton}
              onPress={() => props.navigation.navigate('Category', { title: 'AirPod Cases', id: 32 })}>
              <Text style={styles.drawerText}>AirPod Cases</Text>
              <Image
                style={styles.drawerImage}
                source={{
                  uri: 'https://richlabels.com/wp-content/uploads/2020/05/76.jpg ',
                }} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.drawerButton}
              onPress={() => props.navigation.navigate('Category', { title: 'Bags', id: 116 })}>
              <Text style={styles.drawerText}>Bags</Text>
              <Image
                style={styles.drawerImage}
                source={{
                  uri: 'https://richlabels.com/wp-content/uploads/2020/05/814.jpg',
                }} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.drawerButton}
              onPress={() => props.navigation.navigate('Category', { title: 'Wallets', id: 51 })}>
              <Text style={styles.drawerText}>Wallets</Text>
              <Image
                style={styles.drawerImage}
                source={{
                  uri: 'https://richlabels.com/wp-content/uploads/2020/05/wallet17.jpg',
                }} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.drawerButton}
              onPress={() => props.navigation.navigate('Category', { title: 'Phone Cases', id: 52 })}>
              <Text style={styles.drawerText}>Phone Cases</Text>
              <Image
                style={styles.drawerImage}
                source={{
                  uri: 'https://richlabels.com/wp-content/uploads/2020/05/61.jpg',
                }} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.drawerButton}
              onPress={() => props.navigation.navigate('Category', { title: 'Card Holders', id: 34 })}>
              <Text style={styles.drawerText}>Card Holders</Text>
              <Image
                style={styles.drawerImage}
                source={{
                  uri: 'https://richlabels.com/wp-content/uploads/2020/05/ka7.jpg',
                }} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.drawerButton}
              onPress={() => props.navigation.navigate('Category', { title: 'Lighters', id: 125 })}>
              <Text style={styles.drawerText}>Lighters</Text>
              <Image
                style={styles.drawerImage}
                source={{
                  uri: 'https://richlabels.com/wp-content/uploads/2020/05/814-2-750x750.jpg',
                }} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.drawerButton}
              onPress={() => props.navigation.navigate('Category', { title: 'Clutches', id: 124 })}>
              <Text style={styles.drawerText}>Clutches</Text>
              <Image
                style={styles.drawerImage}
                source={{
                  uri: 'https://richlabels.com/wp-content/uploads/2020/05/420-1-750x750.jpg',
                }} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.drawerButton}
              onPress={() => props.navigation.navigate('Category', { title: 'Chargers', id: 101 })}>
              <Text style={styles.drawerText}>Chargers</Text>
              <Image
                style={styles.drawerImage}
                source={{
                  uri: 'https://richlabels.com/wp-content/uploads/2020/05/Charger16.jpg',
                }} />
            </TouchableOpacity>
          </ScrollView>
        </View>
      );
    }}>
      <Drawer.Screen name="Setting" component={AppBottomNavigator} options={{ headerShown: null }} />
      <Drawer.Screen name="Category" component={Category} options={{ headerShown: null }} />
    </Drawer.Navigator>
  );
}

function AppNavigator() {
  return (
    <NavigationContainer>
      <AppDrawerNavigator />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  drawerButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10
  },

  drawerText: {
    marginTop: 50,
    fontWeight: 'bold',
    width: 100
  },

  drawerImage: {
    width: 120,
    height: 120
  }
});
export default AppNavigator;






