import React, { useState, useEffect} from 'react';
import { View, Text, TextInput, ScrollView, StyleSheet } from 'react-native';
import {
  Content,
  Item,
  Input,
  H3,
  Form,
  Label,
  Icon
} from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import HeaderBar from './Common/HeaderBar';
import { useIsFocused } from '@react-navigation/native';

function ContactUs(props) {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');

  const isFocused = useIsFocused();

  async function sendData() {
      var data = new FormData();
      data.append('your-name', name);
      data.append('your-email', email);
      data.append('your-subject', subject);
      data.append('your-message', message);

      fetch("https://richlabels.com/wp-json/contact-form-7/v1/contact-forms/4026/feedback", {
        method: "POST",
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: data
      })
        .then(response => {
          return response.json();
        })
        .then(responseJson => {
          alert(responseJson.message);

          setName('');
          setEmail('');
          setSubject('');
          setMessage('');

        }).catch(err => {
          alert(err)
        });
  }

  useEffect(() => {
    console.log('Contact Us');
  }, [isFocused]);

  return (
    <ScrollView style={{ flex: 1, backgroundColor: '#ffffff' }}>
      <HeaderBar props={props} />
      <Content padder>
        <H3 style={{ alignSelf: 'center', fontWeight: 'bold' }}>Contact Us</H3>
        <Form>
          <View style={{ marginTop: 20 }}>
            <Item>
              <Icon
                name="user"
                type="FontAwesome"
                style={{ fontSize: 20, color: 'black', marginLeft: -10 }}
              />
              <Label style={{ marginLeft: 5 }}>Full Name</Label>
            </Item>
            <TextInput
              style={styles.input}
              autoCapitalize="words"
              textContentType="name"
              outline="none"
              onChangeText={text => setName(text)}
              value={name}
            ></TextInput>
          </View>
          <View style={{ marginTop: 20 }}>
            <Item>
              <Icon
                name="email"
                type="Entypo"
                style={{ fontSize: 20, color: 'black', marginLeft: -10 }}
              />
              <Label style={{ marginLeft: 5 }}>Email</Label>
            </Item>
            <TextInput
              style={styles.input}
              autoCapitalize="none"
              textContentType="emailAddress"
              outline="none"
              onChangeText={text => setEmail(text)}
              value={email}
            ></TextInput>
          </View>
          <View style={{ marginTop: 20 }}>
            <Item>
              <Icon
                name="subject"
                type="MaterialIcons"
                style={{ fontSize: 20, color: 'black', marginLeft: -10 }}
              />

              <Label style={{ marginLeft: 5 }}>Subject</Label>
            </Item>
            <TextInput
              style={styles.input}
              autoCapitalize="words"
              textContentType="none"
              outline="none"
              onChangeText={text => setSubject(text)}
              value={subject}
            ></TextInput>
          </View>
          <View style={{ marginTop: 20 }}>
            <Item>
              <Icon
                name="message"
                type="Entypo"
                style={{ fontSize: 20, color: 'black', marginLeft: -10 }}
              />
              <Label style={{ marginLeft: 5 }}>Message</Label>
            </Item>
            <TextInput
              style={{
                borderColor: 'black',
                borderWidth: 1,
                borderRadius: 10,
                height: 100,
              }}
              multiline
              autoCapitalize="sentences"
              textContentType="none"
              outline="none"
              onChangeText={text => setMessage(text)}
              value={message}
            ></TextInput>
          </View>
          <View style={{ alignItems: 'center', marginTop: 20 }}>
            <TouchableOpacity
              style={{
                width: 200,
                backgroundColor: 'black',
                borderRadius: 5
              }}
              onPress={() => sendData()}
            >
              <Text style={{ color: 'white', alignSelf: 'center', paddingTop: 10, paddingBottom: 10, fontWeight: 'bold' }}>Submit</Text>
            </TouchableOpacity>
          </View>
        </Form>
      </Content>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  input: {
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 10,
    height: 45,
  }
});

export default ContactUs;
