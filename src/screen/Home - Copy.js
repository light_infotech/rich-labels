import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {
  Container,
  Header,
  Content,
  H2,
  H1,
  Card,
  Left,
  Item,
  Input,
  Segment,
  Right,
  Button,
  Icon,
  Title,
  CardItem,
  Body,
  DeckSwiper,
  Thumbnail, H3
} from 'native-base';

import { connect } from 'react-redux';
import { getAllProducts } from '../redux/actions/productsAction';

let nickImg = require('../assets/products/4.jpg');
 function Home(props) {
  const [isToggled, setToggled] = useState(false);
  const [state, setState] = useState({
    category:"all",
    data: [
      {
        name: 'shits ',
        category: 'men',
        image: require('../assets/products/1.jpg'),
      },
      {
        name: 'shoes',
        category: 'women',
        image: require('../assets/products/2.jpg'),
      },
      {
        name: 'air pod',
        category: 'boys',
        image: require('../assets/products/3.jpg'),
      },
      {
        name: 'bags',
        category: 'girls',
        image: require('../assets/products/4.jpg'),
      },
    ],
  });
  useEffect(()=>{
props.getAllProducts()
  },[])
  const SelectedCateGory = (value)=>{
    console.log('selected value', value)
    setState((state)=>({
...state , category:value
    }))
  }
  let productLists = props.products.map((item, i) => {
    return (
      <View key={i} style={{flex: 1}}>
        <Card style={{borderRadius: 30}}>
          <CardItem
            style={{
              flexDirection: 'column',
              display: 'flex',
              flexDirection: 'row',
              borderRadius: 30,
              elevation: 4,
              height: 120,
            }}>
            <H2
              style={{
                position: 'absolute',
                marginLeft: 10,
                fontWeight: 'bold',
                zIndex: 1,
              }}>
              {item.name} 
            </H2>
            <Image
              source={item.image}
              style={{
                width: '55%',
                height: 100,
                borderRadius: 30,
                position: 'absolute',
                right: 0,
              }}
            />
          </CardItem>
        </Card>
      </View>
    );
  });
  let productListHorizantal = props.products.map((item, i) => {
    return (
      <View key={i} style={{flex: 1}}>
        <Card style={{borderRadius: 30}}>
          <CardItem
            style={{
              flexDirection: 'column',
              display: 'flex',
              flexDirection: 'row',
              // borderRadius: 30,
              elevation: 4,
              height: 150,
              width: 150,
            }}>
            <H2
              style={{
                position: 'absolute',
                marginLeft: 15,
                // fontWeight: 'bold',
                zIndex: 1,
                bottom: 0,
                fontSize:14
              }}>
              {item.name}
            </H2>
            <Image
              source={item.image}
              style={{width: 120, height: 100, borderRadius: 30}}
            />
          </CardItem>
        </Card>
      </View>
    );
  });
  const toggleTrueFalse = () => setToggled(!isToggled);

  return (
    <View style={{flex: 1}}>
      <Header style={{backgroundColor: '#FFFFFF'}}>
        <Left>
          <Button transparent onPress={() => props.navigation.openDrawer()}>
            <Icon name="bars" type="FontAwesome5" style={{color: 'black'}} />
          </Button>
        </Left>
        <Body>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Shop </Text>
        </Body>
        <Right>
          {isToggled === true ? (
            <Item>
              <Icon name="search" type="FontAwesome5" style={{fontSize: 16}} />
              <Input placeholder="Search" />
              <Icon
                onPress={() => toggleTrueFalse()}
                name="times"
                type="FontAwesome5"
                style={{fontSize: 16}}
              />
            </Item>
          ) : (
            <View style={{display: 'flex', flexDirection: 'row'}}>
              <Icon
                onPress={() => toggleTrueFalse()}
                name="search"
                type="FontAwesome5"
                style={{marginRight: 25, fontSize: 16}}
              />
              <Icon
                name="shopping-cart"
                type="FontAwesome5"
                style={{fontSize: 16}}
                onPress={()=>props.navigation.navigate('card')}
              />
            </View>
          )}
        </Right>
      </Header>

      <View style={{alignItems: 'center'}}>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          style={{height: 40, marginTop: 10, marginBottom: 10}}>
         
         
         
         <TouchableOpacity  onPress={()=>SelectedCateGory("all")}
          style={{
            borderRadius: 10,
            backgroundColor: 'white',
            justifyContent: 'center',
          
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>ALL </Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=>SelectedCateGory('phone_case')}
          style={{ 
            borderRadius: 10,
            backgroundColor: 'white',
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>Phone Case</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=>SelectedCateGory('airpod_case')}
          style={{
            borderRadius: 10,
            backgroundColor: 'white',
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>Airpod Cases</Text>
        </TouchableOpacity>

        <TouchableOpacity  onPress={()=>SelectedCateGory('wallet')}
          style={{
            borderRadius: 10,
            backgroundColor: 'white',
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>Wallet</Text>
        </TouchableOpacity>
        <TouchableOpacity  onPress={()=>SelectedCateGory('card_holder')}
          style={{ 
            borderRadius: 10,
            backgroundColor: 'white',
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>Card Holder</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>SelectedCateGory('phone_case')}
          style={{
            borderRadius: 10,
            backgroundColor: 'white',
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}>Phone Case Bags</Text>
        </TouchableOpacity>
        <TouchableOpacity  onPress={()=>SelectedCateGory('Watch_band')}
          style={{ 
            borderRadius: 10,
            backgroundColor: 'white',
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}> Watch Band</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            borderRadius: 10,
            backgroundColor: 'white',
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <Text style={{marginLeft: 10, marginRight: 10}}> Accessoires</Text>
        </TouchableOpacity>
        </ScrollView>
      </View>
      <Content padder>
        {/* <View>{productLists}</View> */}
        {state.category === "all"  ? <View   style={{  flexDirection: "row", flexWrap: 'wrap',  alignItems: "flex-start" }}>
          {props.products.map((item, i) =>
           <TouchableOpacity key={i} style={{backgroundColor:'white' , margin: 5, width:'100%' , elevation:6 , borderRadius:30}} onPress={()=> props.navigation.navigate('Single', {Show:item})}>
          <CardItem
            style={{
              flexDirection: 'column',
              display: 'flex',
              flexDirection: 'row',
              borderRadius: 30,
              elevation: 4,
              height: 120,
            }}>
            <H2
              style={{
                position: 'absolute',
                marginLeft: 10,
                fontWeight: 'bold',
                zIndex: 1,  fontSize:16
              }}>
              {item.name} 
            </H2>
            <Image
              source={item.image}
              style={{
                width: '55%',
                height: 100,
                borderRadius: 30,
                position: 'absolute',
                right: 0,
              }}
            />
          </CardItem>
      
            </TouchableOpacity>
         
           )}
      </View>  :
       <View   style={{  flexDirection: "row", flexWrap: 'wrap',  alignItems: "flex-start" }}>
       {props.products.filter(i => i.category === state.category ).map((item, i) =>
   <TouchableOpacity key={i} style={{backgroundColor:'white' , margin: 5, width:'100%' , elevation:6 , borderRadius:30}} onPress={()=> props.navigation.navigate('Single', {Show:item})}>
   <CardItem
     style={{
       flexDirection: 'column',
       display: 'flex',
       flexDirection: 'row',
       borderRadius: 30,
       elevation: 4,
       height: 120,
     }}>
     <H2
       style={{
         position: 'absolute',
         marginLeft: 10,
         fontWeight: 'bold',
         zIndex: 1,
         fontSize:16
       }}>
       {item.name} 
     </H2>
     <Image
       source={item.image}
       style={{
         width: '55%',
         height: 100,
         borderRadius: 30,
         position: 'absolute',
         right: 0,
       }}
     />
   </CardItem>

     </TouchableOpacity>
  
      
        )}
   </View> }
        <View style={{marginTop: '8%'}}>
          <H1 style={{marginLeft: 20, fontWeight: 'bold', fontSize:22}}>Shop Collections</H1>
          <Content horizontal={true} showsHorizontalScrollIndicator={false}>{productListHorizantal}</Content>
        </View>
        <View style={{marginTop: '8%'}}>
          <H1 style={{marginLeft: 20, fontWeight: 'bold' ,fontSize:22}}>My Interest</H1>
          <TouchableOpacity onPress={()=>props.navigation.navigate('home')}>
            <ImageBackground
              source={nickImg}
              style={{width: '100%', height: 180 , justifyContent:'center'}}>
              <Text style={{position:'absolute', alignSelf:'center', fontSize:30, color:'white', fontWeight:'bold'}}>For your Interest</Text>
            </ImageBackground>
          </TouchableOpacity>
        </View>
      </Content>
      {/* <ScrollView> {productLists} </ScrollView> */}
    </View>
  );
}
const mapStateToProps = state => {
  return {
    products: state.products.products,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getAllProducts: () => dispatch(getAllProducts()),
    
  };
};
export default connect(mapStateToProps , mapDispatchToProps)(Home);