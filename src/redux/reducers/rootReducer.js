import { combineReducers } from 'redux'
import auth from './authReducer'
import modeReducer from './modeReducer'
import events from './eventsReducer';
import clubs from './clubsReducer';
import products from './ProductReducer';

export default combineReducers({
    auth,
    events,
    clubs,
    modeReducer , products
}) 