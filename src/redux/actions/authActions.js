import axios from "axios";
import setAuthToken from "../../utils/setAuthToken";
 import setBasePath from "../../utils/setBasePath";

import AsyncStorage from '@react-native-community/async-storage'

// Login - get user token
export const loginUser = Data => dispatch => {
   setBasePath()
      console.log("data login", Data);
  axios 
    .post("/login", Data)
    .then(res => {
      
      if (res.data.success == false) {
        console.log('login fail')
         dispatch({ type: "LOGIN_ERROR", msg: "Server Error" });
      } else {
        // Set token to localStorage
         const token = res.data.token;
         console.log("success", res.data.user)
         AsyncStorage.setItem("User", JSON.stringify(res.data.user));
         AsyncStorage.setItem("Token", token);
        setAuthToken(token);
        dispatch({ type: "LOGIN_SUCCESS", user: res.data.user });
      }
    })
    .catch(err => {
      //  console.log("helloe man: ", err);
       dispatch({ type: "LOGIN_ERROR", msg: "Invalid Credentials" });
    });
};
// Login - get user token
export const Register = Data => dispatch => {
  setBasePath()
     console.log("data signup", );
 axios 
   .post("/register", Data)
   .then(res => {
     if (res.data.success == false) {
       console.log('sign fail')
        dispatch({ type: "SIGNUP_ERROR", msg: res.data.message });
     } else {
       // Set token to localStorage
        const token = res.data.token;
        console.log('res success', res.data)
        dispatch({ type: "SIGNUP_SUCCESS", msg: res.data.message[0] });
     }
   })
   .catch(err => {
      console.log("signup error ", err);
      // dispatch({ type: "SIGNUP_ERROR", msg: "Server Error" });
   });
};
// Set logged in user
export const setCurrentUser = Data => dispatch => {
  // console.log("data", Data);
  dispatch({ type: "LOGIN_SUCCESS", user: Data });
  // dispatch({ type: "GET_CURRENT_USER", userName: "work" });
};
// Log user out
export const logOutUser = Data => dispatch => {
  // Remove token from local storage
  AsyncStorage.clear();
  // Remove auth header for future requests
  setAuthToken(false);
  // Set isAuthenticated to false
  dispatch({ type: "LOGOUT_USER" });
};




