import React, { useState, useEffect, useRef } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    TextInput,
    Text,
    Alert,
    ScrollView,
    Platform,
    ActivityIndicator,
    Image
} from 'react-native';
import { H3 } from 'native-base';
import HeaderBar from './Common/HeaderBar';
import CartItems from './Common/CartItems';
import WooCommerceAPI from 'react-native-woocommerce-api';
import { useIsFocused } from '@react-navigation/native';
import KlarnaPaymentView from "react-native-klarna-inapp-sdk";
import { NativeModules } from 'react-native';
import axios from 'axios';
import base64 from 'react-native-base64';
import {
    requestOneTimePayment,
    requestBillingAgreement,
    PaypalResponse,
} from 'react-native-paypal';

const authHeader = 'Basic ' + base64.encode(`K786761_3067d57d008e:vpA2maQRXomOp3js`);

var apiCall = new WooCommerceAPI({
    url: 'https://richlabels.com',
    ssl: true,
    consumerKey: 'ck_9718a31d843961b38b01a3361d7a804352bb435b',
    consumerSecret: 'cs_89192ce144dd9d287cf224fea9a14d22946733c7',
    wpAPI: true,
    version: 'wc/v3',
    queryStringAuth: true
});
const paymentMethods = ['pay_now', 'pay_later', 'pay_over_time'];
let authToken = 'myAuthTokenGoesHere';
var  token = "sandbox_mfds5pgs_2r942rmjxthkzg3s";
var key_secret = "EOFceYpEkqhM254lbWmsLZaVaFLnQjP4W5IpILRkjROTfE0pFlkXLc6Jpn_M7DoxXI7ErgGiQP0MWxpB";
var key_id = "AcgQEDfAdAQ2a2XgrcouffXsgaX2Ujs5GtmntWSWxuquTFVCIgS6BwJNdTBhIKMmJv7YDttU8HsD-0gk";

function OrderDetails(props) {

    const { lineItems, total } = props.route.params;

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [firstAddress, setFirstAddress] = useState('');
    const [secondAddress, setSecondAddress] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    const [postcode, setPostcode] = useState('');
    const [country, setCountry] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    let [paymentMethod, setPaymentMethod] = useState("paypal");
    let [createSessionToken, setCreateSessionToken] = useState(false);
    let [createPaypalSessionToken, setPaypalCreateSessionToken] = useState(false);
    let [createKlarnaSessionToken, setKlarnaCreateSessionToken] = useState(false);
    let [paymentViewLoaded, setPaymentViewLoaded] = useState(false);
    

    const isFocused = useIsFocused();
    let myRefKlarnaPaymentView = useRef(null);

    async function createOrder() {

        // myRefKlarnaPaymentView.current.initialize(authToken, 'returnUrl://');
        

        if (firstName == '' || lastName == '' || firstAddress == '' || city == '' ||
            state == '' || postcode == '' || country == '' || email == '' || phone == '') {
        } else {
            // var headerSend = {
            //     "Content-Type":`application/json`,
            //     "Authorization":'Basic ' + base64.encode(`${key_id}:${key_secret}`),
            //     "Accept-Language":"en_GB"
            // };
            // const config = {
            //     headers: headerSend,
            // }
            // // console.log(config, body);
            // const { data } = await axios.post("https://api-m.sandbox.paypal.com/v1/oauth2/token", "grant_type=client_credentials", config).catch(error => {
            //     console.log(error)
            // });
            // token = data.access_token;
            // console.log(token);
            // console.log(data);
            if(paymentMethod === "paypal")
            {
                setPaypalCreateSessionToken(true);
                var amount = total.toString();
                // return false;
                requestOneTimePayment(token, {amount})
                .then((data) => {
                    console.log(data);
                    setPaypalCreateSessionToken(false);
                    alert("Paypal Payment Success!");
                })
                .catch((err) => {
                    console.log(err);
                    setPaypalCreateSessionToken(false);
                });
            }
            else if(paymentMethod === "klarna_paynow" )
            {
                setKlarnaCreateSessionToken(true);
            
                let productItems = [], tempObj = {};
                for (let index = 0; index < lineItems.length; index++) {
                    tempObj['type'] = "physical";
                    tempObj['reference'] = lineItems[index]['product_id'];
                    tempObj['unit_price'] = lineItems[index]['price'];
                    tempObj['tax_rate'] = 0;
                    tempObj['total_amount'] = parseFloat(lineItems[index]['subtotal']);
                    tempObj['total_discount_amount'] = 0;
                    tempObj['total_tax_amount'] = 0;
                    tempObj['name'] = lineItems[index]['name'];
                    tempObj['quantity'] = lineItems[index]['quantity'];
                    // add product url below
                    tempObj['image_url'] = "https://www.exampleobjects.com/logo.png";
                    tempObj['product_url'] = "https://www.estore.com/products/f2a8d7e34";
                    productItems.push(tempObj)
                }

                console.log("Products",{
                    "purchase_country": "GB",
                    "purchase_currency": "GBP",
                    "locale": "en-GB",
                    "order_amount": parseFloat(total-3.55),
                    "order_tax_amount": 0,
                    "order_lines": productItems
                });
                axios.post('https://api.klarna.com/payments/v1/sessions', {
                        "purchase_country": "GB",
                        "purchase_currency": "GBP",
                        "locale": "en-GB",
                        "order_amount": parseFloat(total-3.55),
                        "order_tax_amount": 0,
                        "order_lines": productItems
                    },{
                        headers: {
                            'Authorization': authHeader,
                            "Content-Type": "application/json"
                        }
                    }
                )
                .then(function (response) {
                    console.log(response.data);
                    setKlarnaCreateSessionToken(false);
                    myRefKlarnaPaymentView.current.initialize(response.data.client_token, 'returnUrl://');
                })
                .catch(function (error) {
                    console.log(error);
                    alert("Token can't created")
                    setKlarnaCreateSessionToken(false);
                });
            }else if(paymentMethod === "stripe"){
                //stripe mentod goes here
                
            }

            // const data = {
            //     payment_method: "bacs",
            //     payment_method_title: "Direct Bank Transfer",
            //     currency: "EUR",
            //     status: "pending",
            //     total: total.toString(),
            //     billing: {
            //         first_name: firstName,
            //         last_name: lastName,
            //         address_1: firstAddress,
            //         address_2: secondAddress,
            //         city: city,
            //         state: state,
            //         postcode: postcode,
            //         country: country,
            //         email: email,
            //         phone: phone
            //     },
            //     shipping: {
            //         first_name: firstName,
            //         last_name: lastName,
            //         address_1: firstAddress,
            //         address_2: secondAddress,
            //         city: city,
            //         state: state,
            //         postcode: postcode,
            //         country: country
            //     },
            //     line_items: lineItems,
            //     shipping_lines: [
            //         {
            //             method_id: "flat_rate",
            //             method_title: "Flat Rate",
            //             total: "3.55"
            //         }
            //     ]
            // };
            // apiCall.post("orders", data)
            //     .then((response) => {
            //         Alert.alert(
            //             'Order Created Successfully',
            //             'Order#: ' + response.number,
            //             [
            //                 {
            //                     text: "OK", onPress: () => {

            //                         CartItems.splice(0, CartItems.length);

            //                         setFirstName('');
            //                         setLastName('');
            //                         setFirstAddress('');
            //                         setSecondAddress('');
            //                         setCity('');
            //                         setState('');
            //                         setPostcode('');
            //                         setCountry('');
            //                         setEmail('');
            //                         setPhone('');
            //                         props.navigation.navigate("payment");
            //                     }
            //                 }
            //             ]
            //         );
            //     })
            //     .catch((error) => {
            //         console.log(error.response.data);
            //     });
        }
    }
    const onInitialized = () => {
        myRefKlarnaPaymentView.current.load();
    }
    const onLoaded = () => {
        setPaymentViewLoaded(true)
        myRefKlarnaPaymentView.current.authorize()
    }
    const onAuthorized = (event) => {
        let params = event.nativeEvent
        
        if (params.authorized) {
            console.log(params);
            // submit your form here
        }
    }

    useEffect(() => {
        console.log('Order Details');
      }, [isFocused]);

    return (
        <View style={styles.container}>
            <HeaderBar props={props} />
            <H3 style={{ marginTop: 15, marginBottom: 0, alignSelf: 'center', fontWeight: 'bold' }}>Order Details</H3>
            
            <ScrollView style={styles.form}>
                {
                    paymentViewLoaded ? null : <View>
                    <View style={styles.inputView}>
                        <View style={styles.nestedInput}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.inputTitle}> First Name: </Text>
                                <Text style={styles.mandatory}> * </Text>
                            </View>
                            <TextInput
                                style={styles.input}
                                autoCapitalize="none"
                                outline="none"
                                onChangeText={text => setFirstName(text)}
                            ></TextInput>

                        </View>

                        <View style={styles.nestedInput}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.inputTitle}> Last Name: </Text>
                                <Text style={styles.mandatory}> * </Text>
                            </View>
                            <TextInput
                                style={styles.input}
                                autoCapitalize="none"
                                onChangeText={text => setLastName(text)}
                            ></TextInput>
                        </View>
                    </View>


                    <View style={styles.inputView}>
                        <View style={styles.nestedInput}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.inputTitle}> Address 1: </Text>
                                <Text style={styles.mandatory}> * </Text>
                            </View>
                            <TextInput
                                style={styles.input}
                                autoCapitalize="none"
                                outline="none"
                                onChangeText={text => setFirstAddress(text)}
                            ></TextInput>

                        </View>

                        <View style={styles.nestedInput}>
                            <Text style={styles.inputTitle}> Address 2: </Text>
                            <TextInput
                                style={styles.input}
                                autoCapitalize="none"
                                onChangeText={text => setSecondAddress(text)}
                            ></TextInput>
                        </View>
                    </View>

                    <View style={styles.inputView}>
                        <View style={styles.nestedInput}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.inputTitle}> City: </Text>
                                <Text style={styles.mandatory}> * </Text>
                            </View>
                            <TextInput
                                style={styles.input}
                                autoCapitalize="none"
                                outline="none"
                                onChangeText={text => setCity(text)}
                            ></TextInput>

                        </View>

                        <View style={styles.nestedInput}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.inputTitle}> State: </Text>
                                <Text style={styles.mandatory}> * </Text>
                            </View>
                            <TextInput
                                style={styles.input}
                                autoCapitalize="none"
                                onChangeText={text => setState(text)}
                            ></TextInput>
                        </View>
                    </View>

                    <View style={styles.inputView}>
                        <View style={styles.nestedInput}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.inputTitle}> Postcode: </Text>
                                <Text style={styles.mandatory}> * </Text>
                            </View>
                            <TextInput
                                style={styles.input}
                                autoCapitalize="none"
                                outline="none"
                                onChangeText={text => setPostcode(text)}
                            ></TextInput>
                        </View>

                        <View style={styles.nestedInput}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.inputTitle}> Country: </Text>
                                <Text style={styles.mandatory}> * </Text>
                            </View>
                            <TextInput
                                style={styles.input}
                                autoCapitalize="none"
                                onChangeText={text => setCountry(text)}
                            ></TextInput>
                        </View>
                    </View>


                    <View style={styles.inputView}>
                        <View style={styles.nestedInput}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.inputTitle}> Email: </Text>
                                <Text style={styles.mandatory}> * </Text>
                            </View>
                            <TextInput
                                style={styles.input}
                                autoCapitalize="none"
                                outline="none"
                                onChangeText={text => setEmail(text)}
                            ></TextInput>

                        </View>

                        <View style={styles.nestedInput}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.inputTitle}> Phone: </Text>
                                <Text style={styles.mandatory}> * </Text>
                            </View>
                            <TextInput
                                style={styles.input}
                                autoCapitalize="none"
                                onChangeText={text => setPhone(text)}
                            ></TextInput>
                        </View>
                    </View>
                </View>
                }
                <KlarnaPaymentView
                    style={{
                        flexGrow: 1,
                        height: paymentViewLoaded ? 500 : null
                    }}
                    category={paymentMethods[0]}
                    ref={myRefKlarnaPaymentView}
                    onInitialized={onInitialized}
                    onLoaded={onLoaded}
                    onAuthorized={onAuthorized}
                    onError={(error) => {
                        console.log(error.nativeEvent.error);
                    }}
                />
                {!paymentViewLoaded &&
                <View style={{ marginBottom: 20,marginHorizontal:10,}}>
                    <TouchableOpacity onPress={() => setPaymentMethod("paypal") } style={{flex: 1,flexDirection: "row",alignItems: "center",paddingVertical: 10}}>
                        <View style={{flex: 0.15,}}>
                            <View style={{height: 30,width: 30,backgroundColor: "lightgrey",borderRadius: 15,justifyContent: "center",alignItems: "center"}}>
                                { paymentMethod === "paypal" ? <View style={{backgroundColor: "black",height: 20,width: 20,borderRadius: 15}} /> : null}
                            </View>
                        </View>
                        <View style={{flex: 0.85}}>
                            <Text style={{fontSize: 16,fontWeight: "bold"}}>PayPal</Text>
                            <Image style={{height: 50,width: 150}} resizeMode="contain" source={{ uri: "https://www.paypalobjects.com/digitalassets/c/website/marketing/apac/IN/logo-center/logo-center-solution-graphics.png" }} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setPaymentMethod("klarna_paynow") } style={{flex: 1,flexDirection: "row",alignItems: "center",paddingVertical: 10}}>
                        <View style={{flex: 0.15,}}>
                            <View style={{height: 30,width: 30,backgroundColor: "lightgrey",borderRadius: 15,justifyContent: "center",alignItems: "center"}}>
                                { paymentMethod === "klarna_paynow" ? <View style={{backgroundColor: "black",height: 20,width: 20,borderRadius: 15}} /> : null}
                            </View>
                        </View>
                        <View style={{flex: 0.85,}}>
                            <Text style={{fontSize: 16,fontWeight: "bold"}}>Klarna (Pay Now)</Text>
                            <Image style={{height: 50,width: 60}} resizeMode="contain" source={{uri: "https://x.klarnacdn.net/payment-method/assets/badges/generic/klarna.png"}} />
                        </View>
                    </TouchableOpacity>
                    {/* <TouchableOpacity onPress={() => setPaymentMethod("klarna_payletter") } style={{flex: 1,flexDirection: "row",alignItems: "center",paddingVertical: 10}}>
                        <View style={{flex: 0.15,}}>
                            <View style={{height: 30,width: 30,backgroundColor: "lightgrey",borderRadius: 15,justifyContent: "center",alignItems: "center"}}>
                                { paymentMethod === "klarna_payletter" ? <View style={{backgroundColor: "black",height: 20,width: 20,borderRadius: 15}} /> : null}
                            </View>
                        </View>
                        <View style={{flex: 0.85,}}>
                            <Text style={{fontSize: 16,fontWeight: "bold"}}>Klarna (Pay Letter)</Text>
                            <Image style={{height: 50,width: 60}} resizeMode="contain" source={{uri: "https://x.klarnacdn.net/payment-method/assets/badges/generic/klarna.png"}} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setPaymentMethod("klarna_paysliceit") } style={{flex: 1,flexDirection: "row",alignItems: "center",paddingVertical: 10}}>
                        <View style={{flex: 0.15,}}>
                            <View style={{height: 30,width: 30,backgroundColor: "lightgrey",borderRadius: 15,justifyContent: "center",alignItems: "center"}}>
                                { paymentMethod === "klarna_paysliceit" ? <View style={{backgroundColor: "black",height: 20,width: 20,borderRadius: 15}} /> : null}                                
                            </View>
                        </View>
                        <View style={{flex: 0.85,}}>
                            <Text style={{fontSize: 16,fontWeight: "bold"}}>Klarna (Slice it)</Text>
                            <Image style={{height: 50,width: 60}} resizeMode="contain" source={{uri: "https://x.klarnacdn.net/payment-method/assets/badges/generic/klarna.png"}} />
                        </View>
                    </TouchableOpacity> */}
                    <TouchableOpacity onPress={() => setPaymentMethod("stripe") } style={{flex: 1,flexDirection: "row",alignItems: "center",paddingVertical: 10}}>
                        <View style={{flex: 0.15,}}>
                            <View style={{height: 30,width: 30,backgroundColor: "lightgrey",borderRadius: 15,justifyContent: "center",alignItems: "center"}}>
                                { paymentMethod === "stripe" ? <View style={{backgroundColor: "black",height: 20,width: 20,borderRadius: 15}} /> : null}                                
                            </View>
                        </View>
                        <View style={{flex: 0.85,}}>
                            <Text style={{fontSize: 16,fontWeight: "bold"}}>Visa/Mastercard/American-Express</Text>
                            <Image style={{height: 50,width: 150}} resizeMode="contain" source={require("./../assets/images/visa-mastercard-amex.png")} />
                        </View>
                    </TouchableOpacity>
                    <View style={{justifyContent: 'center',alignItems: 'center',marginTop: 20,}}>
                        <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={() => createOrder()}>
                            <Text style={styles.loginText}>{createPaypalSessionToken ? <ActivityIndicator color="white" size={28} /> : "Place Order"}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            }
            </ScrollView>
            <View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    inputView: {
        flexDirection: "row",
        flex: 1
    },

    nestedInput: {
        flex: 5,
        margin: 10
    },

    inputContainer: {
        backgroundColor: '#FFFFFF',
        borderColor: '#8A8F9E',
        borderRadius: 4,
        borderWidth: 1,
        width: 300,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },

    form: {
        marginHorizontal: 30
    },
    inputTitle: {
        color: "black",
        fontWeight: "bold",
        fontSize: 13,
    },

    mandatory: {
        color: "red",
        fontWeight: "bold",
        fontSize: 13,
    },

    input: {
        borderBottomColor: "#8A8F9E",
        borderBottomWidth: StyleSheet.hairlineWidth,
        height: 40,
        fontSize: 15,
        color: "#161F3D"
    },
    passwordField: {
        marginTop: 32
    },

    inputs: {
        height: 45,
        marginLeft: 16,
        borderBottomColor: '#FFFFFF',
        flex: 1,
    },
    buttonContainer: {
        flexDirection: 'row',
        borderRadius: 120,
        height: 45,
        width: 150,
        alignItems: "center",
        justifyContent: "center"
    },

    loginButton: {
        backgroundColor: "black",
    },

    loginText: {
        color: 'white',
        fontWeight: "500"
    },
});

export default OrderDetails;