import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {
  Container,
  Header,
  Col,
  Grid,
  Content,
  H2,
  H1,
  Card,
  Left,
  Item,
  Input,
  Segment,
  Right,
  Button,
  Icon,
  Title,
  Thumbnail,
  CardItem,
  Body,
  H3,
  Toast,
} from 'native-base';
let img = require('../assets/products/1.jpg');
import ColorPalette from 'react-native-color-palette';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';

export default function SingleProduct(props) {
  const getVal =async ()=>{
    let existingObj = await AsyncStorage.getItem('SaveItem');
 console.log('existingObj',existingObj)
  }
  useEffect(()=>{
getVal()
  },[])
   const {Show} = props.route.params;
  // console.log('show',  Show)
  const [modalVisible, setModalVisible] = useState(false);

  const [state, setState] = useState({
    selectedColor : '#E74C3C',
    data: [
      {
        name: 'shits',
        category: 'men',
        image: require('../assets/products/1.jpg'),
      },
      {
        name: 'shoes',
        category: 'women',
        image: require('../assets/products/2.jpg'),
      },
      {
        name: 'air pod',
        category: 'boys',
        image: require('../assets/products/3.jpg'),
      },
      {
        name: 'bags',
        category: 'girls',
        image: require('../assets/products/4.jpg'),
      },
    ],
  });
  
  const couponsModel = () => setModalVisible(!modalVisible);
  const showToast = (msg, type) => {
    Toast.show({
      text: msg,
      buttonText: 'Ok',
      duration: 3000,
      type: type,
    });
  };
  const [isToggled, setToggled] = useState(false);
  const toggleTrueFalse = () => setToggled(!isToggled);
const AddToCart = async (e)=>{
  await AsyncStorage.setItem('SaveItem', JSON.stringify(e))

// console.log('add to cart', e)
  let existingObj = await AsyncStorage.getItem('SaveItem');
  existingObj = JSON.parse(existingObj)
  let arr =  [existingObj,e];
  // console.log('array',arr)
  await AsyncStorage.setItem('SaveItem', JSON.stringify(arr))
//     if( existingObj != null){ 

//   existingObj = JSON.parse(existingObj)
//   let arr =  [...existingObj,e];
//   console.log('array',arr)
// //   await AsyncStorage.setItem('SaveItem', JSON.stringify(arr))
//       } else{
//         await AsyncStorage.setItem('SaveItem', JSON.stringify([]))
       
//       }


  console.log('item in cart', Show)
  showToast('addedd to card', 'success')
}
  let productListHorizantal = state.data.map((item, i) => {
    return (
      <View key={i} style={{flex: 1}}>
        <Card style={{borderRadius: 30}}>
          <CardItem
            style={{
              flexDirection: 'column',
              display: 'flex',
              flexDirection: 'row',
              // borderRadius: 30,
              elevation: 4,
              height: 200,
              width: 200,
            }}>
            <Image
              source={item.image}
              style={{width: 150, height: 150, borderRadius: 30}}
            />
          </CardItem>
        </Card>
      </View>
    );
  });
  return (
    <View style={{flex: 1}}>
      <Header style={{backgroundColor: '#FFFFFF'}}>
        <Left>
          <Button
            transparent
            onPress={() => props.navigation.navigate('productList')}>
            <Icon
              name="arrow-left"
              type="FontAwesome5"
              style={{color: 'black'}}
            />
          </Button>
        </Left>
        <Body>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Shop </Text>
        </Body>
        <Right>
          {isToggled === true ? (
            <Item>
              <Icon name="search" type="FontAwesome5" style={{fontSize: 16}} />
              <Input placeholder="Search" />
              <Icon
                onPress={() => toggleTrueFalse()}
                name="times"
                type="FontAwesome5"
                style={{fontSize: 16}}
              />
            </Item>
          ) : (
            <View style={{display: 'flex', flexDirection: 'row'}}>
              <Icon
                onPress={() => toggleTrueFalse()}
                name="search"
                type="FontAwesome5"
                style={{marginRight: 25, fontSize: 16}}
              />
              <Icon
                name="shopping-cart"
                type="FontAwesome5"
                style={{fontSize: 16}}
              />
            </View>
          )}
        </Right>
      </Header>
      <Content padder>
        <View>
          <Image source={img} style={{width: '100%', height: 200}} />
        </View>

        <View style={{flex: 1}}>
          <Content horizontal={true} showsHorizontalScrollIndicator={false}>
            {productListHorizantal}
          </Content>
        </View>
        <View style={{margin: 10}}>
          {/* <H3>{Show.image}</H3> */}
        
          <H1 style={{fontWeight: 'bold'}}>Mobile Case</H1> 
          
          <Text>$100</Text>
          <Text>
            nothing as fly, nothing as confortable, nothing nothing as proven,
            nothing as fly, nothing as confortable, nothing nothing as proven,
            nothing as fly, nothing as confortable, nothing nothing as proven
          </Text>
        </View>
        <View style={{alignItems: 'center'}}>
          <TouchableOpacity
            style={{
              backgroundColor: 'black',
              width: '90%',
              alignItems: 'center',
              borderRadius: 30,
            }}>
            <Text
              style={{
                color: 'white',
                fontSize: 22,
                paddingTop: 10,
                paddingBottom: 10,
              }}
              onPress={(e) => AddToCart(Show)}>
              Add To Bag
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 20,
          }}>
          <TouchableOpacity
            style={{
              borderColor: 'gray',
              borderWidth: 1,
              width: '45%',
              alignItems: 'center',
              borderRadius: 30,
            }}>
            <Text
              style={{
                color: 'black',
                fontSize: 22,
                paddingTop: 10,
                paddingBottom: 10,
              }}
              onPress={() => couponsModel()}>
              Buy Now
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              borderColor: 'gray',
              borderWidth: 1,
              width: '45%',
              alignItems: 'center',
              borderRadius: 30,
            }}
            onPress={() => showToast('addedd to favourit', 'success')}>
            <Icon
              name="heart"
              type="FontAwesome5"
              style={{
                color: 'black',
                fontSize: 30,
                paddingTop: 10,
                paddingBottom: 10,
              }}
            />
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          <Modal
            animationIn={'slideInUp'}
            animationOut={'slideOutDown'}
            animationInTiming={Number(1400)}
            animationOutTiming={Number(1400)}
            backdropColor={'#D3D3D3'}
            isVisible={modalVisible}>
            
            <Content padder>
              <View
                style={{
                  flex: 1,
                  borderRadius: 30,
                  // alignItems: 'center',
                  // justifyContent: 'center',
                }}>
                <View style={styles.modelbg}>
                  <View style={{height: 40}}>
                    <Icon
                      name="minus"
                      type="FontAwesome5"
                      style={{
                        // color: 'red',
                        right: 10,
                        top: 10,
                        position: 'absolute',
                        fontSize: 28,
                      }}
                      onPress={() => couponsModel()}
                    />
                  </View>
                 <View>
                  
                  {/* <ColorPalette
                    // onChange={color => selectedColor = setState((state)=>({...state,selectedColor:color}))}
                    onChange={color=>setState((state)=>({...state,selectedColor:color}))}
                    value={state.selectedColor} 
                    colors={['#C0392B', '#E74C3C', '#9B59B6', '#8E44AD', '#2980B9']}
                    title={"Select the Product color you Like:"}
                    icon={
                      <Icon name='check-circle' type="FontAwesome5" size={12} color={'black'} />
                  }
                /> */}
                 </View>
                  <Item>
                    <Input placeholder="First Name" />
                    <Input placeholder="Last Name" />
                  </Item>
                  <Item>
                    <Input placeholder="Address line 1" />
                  </Item>
                  <Item>
                    <Input placeholder="Address line 2 (optional)" />
                  </Item>
                  <Item>
                    <Input placeholder="City/town" />
                    <Input placeholder="Postal code" />
                  </Item>
                  <Item>
                    <Input placeholder="Phone country" />
                  </Item>
                  <Item>
                    <Input placeholder="First Name" />
                  </Item>
                  <TouchableOpacity
                    style={{
                      backgroundColor: 'black',
                      height: 40,
                      borderRadius: 30,
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: 20,
                    }}>
                    <Text style={{color: 'white'}}>Add Shipping Address</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Content>
          </Modal>
        </View>
      </Content>
    </View>
  );
}
const styles = StyleSheet.create({
  textColor: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontFamily: 'Sarabun-SemiBold',
  },
  textColorBtn: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontFamily: 'Sarabun-SemiBold',
    fontSize: 12,
    textTransform: 'lowercase',
  },
  icon: {
    color: '#FFFFFF',
  },

  textColorBtn: {
    color: 'black',
    fontWeight: 'bold',
    fontFamily: 'Sarabun-SemiBold',
    textTransform: 'lowercase',
  },

  cardItem: {
    backgroundColor: '#1B3853',
    width: '100%',
    borderRadius: 30,
    padding: 20,
    top: 0,
  },
  button: {
    backgroundColor: '#C5C5C5',
    color: '#FFFFFF',
    height: 30,
  },
  check: {
    color: '#BFBFBF',
    fontSize: 12,
    fontWeight: 'bold',
    marginLeft: 10,
  },

  modelbg: {
    backgroundColor: '#FFFFFF',
    width: '99%',
    borderRadius: 30,
  },
  cardProfile: {
    borderRadius: 50,
    borderColor: 'black',
    marginTop: 10,
    marginBottom: 10,
  },
  couponsText: {
    color: '#BFBFBF',
    marginLeft: 5,
  },
});
