import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';
import {
  Container,
  Header,
  Col,
  Grid,
  Content,
  H2,
  H1,
  Card,
  Left,
  Item,
  Input,
  Segment,
  Right,
  Button,
  Icon,
  Title,
  Thumbnail,
  CardItem,
  Body,
} from 'native-base';
import ImagePicker from 'react-native-image-picker';
import avatar from '../assets/avatar.jpg';

function Profile(props) {
  const selectImage = () => {
    const options = {
      title: 'Select Avatar',
      customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        //   console.log('source', response.data)
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        setState((state) => ({
          ...state,
          picture: response.data,
        }));
      }
    });
  };
  return (
    <View>
      <Header style={{backgroundColor: 'white'}}>
        <Left>
          {/* <Icon name="arrow-left" type="FontAwesome5" onPress={()=> props.navigation.goBack()}/> */}
          <Button transparent onPress={() => props.navigation.navigate('home')}>
            <Icon name="arrow-left" type="FontAwesome5" style={{color: 'black'}} />
          </Button>
        </Left>
        <Body>
          <Title style={{color: 'black'}}>Profile Setting</Title>
        </Body>
        <Right />
      </Header>
      <View
        style={{alignItems: 'center', marginTop: '10%', marginBottom: '5%'}}>
        <TouchableOpacity onPress={() => selectImage()}>
          <Image
            source={avatar}
            style={{width: 120, height: 120, borderRadius: 120 / 2}}
          />
        </TouchableOpacity>
        <H2>Mujahid Iqbal</H2>
        <Text>Pakistan</Text>
      </View>
      <View style={{alignItems: 'center', marginTop: '1%'}}>
        <TouchableOpacity
          style={{
            borderColor: '#bbbfbc',
            borderWidth: 1,
            width: 150,
            alignItems: 'center',
          }}>
          <Text style={{paddingTop: 5, paddingBottom: 5}}>Edit Profile</Text>
        </TouchableOpacity>
      </View>
      <View style={{display:'flex', flexDirection:'row', justifyContent:'center', marginTop:'3%'}}>
          <TouchableOpacity onPress={()=>alert('work')}>
        <Card style={{borderRadius:10}}>
          <CardItem  style={{borderRadius:10, flexDirection:'column'}}>
              <Icon name="sort-amount-up" type="FontAwesome5" />
            <Text>Orders</Text>
          </CardItem>
        </Card></TouchableOpacity>
        <TouchableOpacity onPress={()=>alert('work')}>
        <Card style={{borderRadius:10}}>
          <CardItem style={{borderRadius:10, flexDirection:'column'}}>
          <Icon name="passport" type="FontAwesome5" />
            <Text>Pass</Text>
          </CardItem>
        </Card></TouchableOpacity>
        <TouchableOpacity onPress={()=>alert('work')}>
        <Card style={{borderRadius:10}}>
          <CardItem style={{borderRadius:10, flexDirection:'column'}}>
          <Icon name="calendar-week" type="FontAwesome5" />
            <Text>Events</Text>
          </CardItem>
        </Card></TouchableOpacity>
        <TouchableOpacity onPress={()=>alert('work')}>
        <Card style={{borderRadius:10}}>
          <CardItem style={{borderRadius:10, flexDirection:'column'}}>
          <Icon name="cogs" type="FontAwesome5" />
            <Text>Settings</Text>
          </CardItem>
        </Card></TouchableOpacity>
      </View>
      <Button><Text>send</Text></Button>
    </View>
  );
}
export default Profile;
