import React from 'react';
import {View, Dimensions, Text, Linking} from 'react-native';
import {Container, Header, Content, Badge, Icon, H2, Button} from 'native-base';

export default function UpdateLocation(props) {
  return (
    <View
      style={{
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
      }}>
      <View
        style={{
          width: 100,
          height: 100,
          borderColor: 'white',
          borderWidth: 1,
          borderRadius: 100 / 2,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Icon
          name="check"
          type="FontAwesome5"
          style={{color: 'white'}}
        />
      </View>
      <H2 style={{color: 'white', marginTop: 20}}>Update Your Location</H2>
      <Text style={{color: 'white', alignSelf: 'center'}}>
        {' '}
        For a best experience in the nick app
      </Text>
      <Text style={{color: 'white', alignSelf: 'center'}}>
        Select a supported country as your location.
      </Text>
      <Text style={{color: 'white', alignSelf: 'center'}}>
        if your country is not supported the visit
      </Text>

      <Text
        style={{color: 'white', borderBottomColor: 'white', borderWidth: 1}}
        onPress={() => Linking.openURL('http://google.com')}>
        www.nick.com
      </Text>

      <View style={{position: 'absolute', bottom: 20}}>
        <Button
          rounded
          style={{backgroundColor: 'white'}}
          onPress={() => props.navigation.navigate('interest')}>
          <Text style={{paddingLeft: 15, paddingRight: 15}}>Continue</Text>
        </Button>
      </View>
    </View>
  );
}
