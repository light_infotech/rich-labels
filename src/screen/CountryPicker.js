import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import {Item} from 'native-base';
import CountryPicker, {DARK_THEME} from 'react-native-country-picker-modal';
export default function SelectCountry() {
  const [state, setState] = useState({
    countryCode: '',
    country: '',
    withCountryNameButton: false,
  });
  
  const onSelectCountry = (Country) => {
    console.log('country', Country);
    setState((state) => ({
      ...state,
      country: Country,
      countryCode: Country.cca2,
    }));
  };
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: 'black',
      }}>
      {/* <CountryPicker theme={DARK_THEME}/> */}

      <Item
        rounded
        style={{
          height: 50,
          //   marginTop: 20,
          //   paddingLeft: 10,
          backgroundColor: 'white',
          //   borderColor: '#BFFEFE',
          onBackgroundTextColor: '#000000',
          opacity: 1,
          width: '100%',
        }}>
        {/* {state.countryCode!=''?null:<Icon style={{color:"#000000"}} type="Ionicons" name="flag"/>}   */}
        <CountryPicker
          theme={DARK_THEME}
          containerButtonStyle={{paddingLeft: 4}}
          withAlphaFilter={true}
          withFilter={true}
          withFlag={true}
          onSelect={onSelectCountry}
          withCountryNameButton={true}
           countryCode={state.countryCode}
        />
        {state.country !== null && <Text style={{marginLeft:-50}}>{state.country.name}</Text>}
      </Item>
    </View>
  );
}
