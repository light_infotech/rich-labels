import axios from 'axios';

export const getAllEvents = data => dispatch => {
// alert('evt')
    axios
        .post("/all_events")
        .then(res => {
            if (res.data.success == false) {
                // console.log('evt faslse')

                 dispatch({ type: "GET_EVENTS_ERROR", msg: "error" });
            } else {
                // console.log('evt', res.data)
                dispatch({
                    type: "GET_EVENTS_SUCCESS",
                    events: res.data.events,
                });
            }
        })
        .catch(err => {
            // console.log('evt err', err) 

            dispatch({
                type: "GET_EVENTS_ERROR",
                msg: "Error While Fetching Data"
            });
        });
};