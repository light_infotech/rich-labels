
const initState = {
  Error: null,
  mode: false
}
import AsyncStorage from '@react-native-community/async-storage';

const modeReducer = (state = initState, action) => {
  switch (action.type) {
    case 'MODE_ERROR':
      return {
        ...state,
        Error: "Something Went Wrong",
      }
    case 'MODE_SUCCESS_CHANGE':
      return {
        // mode : await AsyncStorage.getItem(('Display')),
        ...state,
        Error: null,
        mode: action.mode
      }
    case 'MODE_GET_FROM_STORAGE':
      return {
        ...state,
        Error: null,
        mode: action.mode
      }
    default:
      return state
  }

}

export default modeReducer 