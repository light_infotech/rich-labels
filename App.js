import React, {useEffect , useState} from 'react';
import {StyleSheet, View, StatusBar , TouchableOpacity, Text} from 'react-native';
import {Root} from 'native-base';
import 'react-native-gesture-handler';
import AppNavigator from './src/navigation/AppNavigator';
import store from './src/redux/store';
import {Provider} from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
//  import Paypal from './src/screen/Paypal';


const App = () => {
 
  useEffect(() => {
    SplashScreen.hide();
  }, []);
 
  return (
    <Provider store={store}>
      <View style={{flex: 1, backgroundColor:'white'}}>
        <StatusBar barStyle="light-content" />
        <Root>
          <AppNavigator />
        {/* <Paypal/> */}
        </Root>
      </View>
    </Provider>
  );
};

export default App;
