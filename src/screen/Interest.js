import React, { Component } from "react";
import { View, Text, Dimensions } from "react-native";
import {
  Container,
  Header,
  Content,
  Badge,
  Icon,
  H2,
  H3,
  Button,
} from "native-base";
export default function Interest(props) {
 
    return (
      <View
        style={{
          backgroundColor: "black",
          alignItems: "center",
          justifyContent: "center",
          width: Dimensions.get("window").width,
          height: Dimensions.get("window").height,
        }}
      >
        <View>
          <H3 style={{ color: "white" }}>Prefered Products</H3>
          <Text style={{ color: "white", alignSelf: "center" }}>
            What type of products are
          </Text>
          <Text style={{ color: "white", alignSelf: "center" }}>
            you more interested in?
          </Text>
        </View>
        <View style={{padding:20}}>
          <Button block  style={{backgroundColor:'white', width:200 , marginBottom:15}}  onPress={()=> props.navigation.navigate('productList')}>
            <Text>Men's</Text>
          </Button>
          <Button block style={{backgroundColor:'black', width:200 , marginBottom:15 , borderColor:"white", borderWidth:1}}  onPress={()=> props.navigation.navigate('productList')}>
            <Text style={{color:'white'}}>Women's</Text>
          </Button>
        </View>
  
        <View  style={{ position: "absolute", bottom: 100 ,  }}>
        <Button onPress={()=> props.navigation.navigate('home')} rounded style={{ backgroundColor: "white",   }}>
          <Text  style={{paddingLeft:25, paddingRight:25}}>Next </Text>
        </Button>
      </View>
      </View>
    );
  
}
