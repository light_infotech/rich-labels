import axios from 'axios';

export const getAllProducts = data => dispatch => {
    // alert('work')
    axios
        .get("https://richlabels.com/wp-json/wc/v3/products")
        .then(res => {
            // console.log('headers', res.headers)
            if (res.data.success == false) {
                console.log('success falls')
                // dispatch({ type: "GET_CLUBS_ERROR", msg: "error" });
            } else {
                 console.log('success products', res.data)
                dispatch({
                    type: "GET_PRODUCTS_SUCCESS",
                    clubs:res.data.clubs ,
               
                });
            }
        })
        .catch(err => {
            console.log('server err', err)
            dispatch({
                type: "GET_PRODUCTS_ERROR",
                msg: "Error While Fetching Data"
            });
        });
};